var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    minifyCSS  = require('gulp-minify-css'),
    rename     = require('gulp-rename'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    connect    = require('gulp-connect'),
    sourcemaps = require('gulp-sourcemaps'),
    iconv      = require('gulp-iconv'),
    jpegoptim  = require('imagemin-jpegoptim'),
    pngquant   = require('imagemin-pngquant'),
    optipng    = require('imagemin-optipng'),
    svgo       = require('imagemin-svgo');

gulp.task('sass', function () {
    return gulp.src('./src/sass/app.sass')
        .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(minifyCSS())
            .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('js', function() {
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        './src/js/**/*.js',
        ])
        .pipe(sourcemaps.init())
            .pipe(concat('app.js'))
            .pipe(uglify())
            .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('copy', function() {
    return gulp.src([
        './bower_components/fontawesome/fonts/*',
        './bower_components/bootstrap-sass/assets/fonts/bootstrap/*',
        ])
        .pipe(gulp.dest('./build/fonts'));
});

gulp.task('img', function () {
    gulp.src('./src/images/**/*.{png,jpg,jpeg,gif,svg}')
        .pipe(pngquant({quality: '65-80', speed: 4})())
        .pipe(optipng({optimizationLevel: 3})())
        .pipe(jpegoptim({max: 100})())
        .pipe(svgo()())
        .pipe(gulp.dest('./build/images'));
});

gulp.task('connect', function() {
  connect.server({
    root: './build',
    livereload: true
  });
});

gulp.task('reload', function () {
  gulp.src('./build/index.html')
    .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch(['./build/**/*.html', './build/js/**/*.js', './build/css/**/*.css'], ['reload']);
    gulp.watch(['./src/sass/**/*.sass', './src/sass/**/*.scss'], ['sass']);
    gulp.watch(['./src/js/**/*.js'], ['js']);
});

gulp.task('run', function() {
    gulp.run('connect', 'watch');
});

gulp.task('cp1251', function () {
    return gulp.src('./build/**/*')
        .pipe(iconv({
            decoding: 'utf-8',
            encoding: 'windows-1251'
        }))
        .pipe(gulp.dest('./build_cp1251/'));
});